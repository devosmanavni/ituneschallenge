//
//  DetailViewController.swift
//  RxHayda
//
//  Created by Rooster on 2.03.2019.
//  Copyright © 2019 Rooster. All rights reserved.
//

import UIKit
import RxSwift

class DetailViewController: UIViewController {
    
    var searchItem:ResultItem?
    
    lazy var detailView: DetailViews  = {
        let  detailView = DetailViews().loadNib() as! DetailViews
        return detailView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(detailView)
        
        if let detailData = searchItem {
            detailView.itemImageView.loadImage(fromUrl:detailData.artworkUrl100 )
            
            detailView.trackNameLabel.text = detailData.getListItemNameText() ?? ""
            detailView.collectionNameLabel.text = detailData.artistName ?? ""
            detailView.subInfo1Label.text = detailData.primaryGenreName ?? ""
            detailView.subInfo2Label.text =  getSubInfo2()
            detailView.subInfo3label.text = String(format: "Release: %@", DateTimeUtils.sharedInstance.dateStringToFormattedString(dateString: detailData.releaseDate))
        }
        else {
            showPopupMessage(titleText: "Error", messageText: "Detail data not found!")
        }
    }
    
    func detailViewSetup() {
        detailView.backgroundColor = UIColor.red
        detailView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        detailView.collectionNameLabel?.text = "skmaskcmaskcakcas smcvskcms skmcvskd"
        detailView.itemImageView?.image = UIImage(named: "osman")
        detailView.subInfo1Label?.text  = " asmfasmasmfas"
        let contraints = [
            detailView.topAnchor.constraint(equalTo: view.bottomAnchor),
            detailView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            detailView.leftAnchor.constraint(equalTo: view.leftAnchor),
            detailView.rightAnchor.constraint(equalTo: view.rightAnchor),
            detailView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ]
        NSLayoutConstraint.activate(contraints)
    }
    
    func getSubInfo2() -> String {
        var subInfo2 = ""
        
        if let kind = searchItem!.kind {
            if kind.contains("movie") {
                subInfo2 = searchItem?.contentAdvisoryRating ?? ""
            } else if (kind.contains("song")) {
                subInfo2 = String(format: "%i Track", searchItem!.trackCount ?? 1)
            } else if (kind.contains("podcast")) {
                subInfo2 = "Podcasts"
            }
        }
        
        return subInfo2
    }
    
    func showPopupMessage(titleText:String?, messageText:String?) {
        let alert = UIAlertController(title: titleText ?? "", message: messageText ?? "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler:{ action in
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
        }))
        
        self.present(alert, animated: true)
    }
}

