//
//  ViewController.swift
//  RxHayda
//
//  Created by Rooster on 1.03.2019.
//  Copyright © 2019 Rooster. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MainViewController: UIViewController {
    private let disposeBag = DisposeBag()
    private let throttleInterval = 0.1
    let customCellIdentifier = "SearchCell"
    let kSearchTypeMenuItems = ["All":"all", "Music":"music", "Movie":"movie", "Podcast":"podcast"]
    
    var repoViewModel:SearchViewModel?
    var apiProvider = ServiceManager()
    
    // Initialization clouse
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = 70
        tableView.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: customCellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    lazy var searchBar : MainSearchBar = {
        let searchBar = MainSearchBar()
        searchBar.returnKeyType = .done
        definesPresentationContext = true
        return searchBar
    }()
    
    lazy var rightBarButton: UIBarButtonItem = {
        let rightBarButton = UIBarButtonItem(title: "All", style: .plain, target: self, action: #selector(rightBarButtonTapped))
        return rightBarButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        createSearchViewModel()
    }
    
    func createSearchViewModel() {
        repoViewModel = SearchViewModel(api: self.apiProvider)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        if let viewModel = repoViewModel {
            
            viewModel.data.drive(tableView.rx.items(cellIdentifier: customCellIdentifier, cellType: SearchTableViewCell.self)) {
                row , item, cell in
                
                cell.nameLabel.text = item.getListItemNameText() ?? ""
                cell.infoLabel.text = item.artistName ?? ""
                cell.setReaded(readed: false)
                cell.itemImageView.loadImage(fromUrl: item.artworkUrl60)
                cell.setReaded(readed: item.isReaded)
                }.disposed(by: disposeBag)
            
            tableView
                .rx // 1
                .modelSelected(ResultItem.self) // 2
                .subscribe(onNext: { [weak self] item in // 3

                    let vc =  DetailViewController()
                    vc.searchItem = item
                    self?.navigationController?.pushViewController(vc, animated: true)
                    if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow { // 5
                        self?.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
                    }
                })
                .disposed(by: disposeBag) // 6
            
            searchBar.rx.text.orEmpty.bind(to: viewModel.searchText).disposed(by: disposeBag)
        }
    }
    
    fileprivate func setupView(){
        view.addSubview(searchBar)
        view.addSubview(tableView)
        
        let contraints = [
            searchBar.topAnchor.constraint(equalTo: view.topAnchor,constant:60),
            searchBar.heightAnchor.constraint(equalToConstant: 70),
            searchBar.leftAnchor.constraint(equalTo: view.leftAnchor),
            searchBar.rightAnchor.constraint(equalTo: view.rightAnchor),
            
            tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ]
        NSLayoutConstraint.activate(contraints)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]
    }
    
    @objc
    func rightBarButtonTapped(){
        print(#function)
        let alert = UIAlertController(title: "Seçim", message: "Görüntüle tipinizi seçiniz.", preferredStyle: .actionSheet)
        for item in kSearchTypeMenuItems.keys.sorted() {
            alert.addAction(UIAlertAction(title: item, style: .default, handler: { alertAction in
                self.rightBarButton.title = alertAction.title
                if let alertTitle = alertAction.title {
                    if let searchType = self.kSearchTypeMenuItems[alertTitle] {
                        self.apiProvider.searchTextType = searchType
                    }
                }
            }))
        }
        
        alert.addAction(UIAlertAction(title: "İptal", style: .destructive, handler:nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
}


