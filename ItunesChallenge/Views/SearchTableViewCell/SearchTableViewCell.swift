//
//  SearchTableViewCell.swift
//  RxHayda
//
//  Created by Rooster on 3.03.2019.
//  Copyright © 2019 Rooster. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var readedOverlayView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setReaded(readed:Bool) {
        readedOverlayView.isHidden = !readed
    }
    
    
    

}
